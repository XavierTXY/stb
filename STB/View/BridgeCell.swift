//
//  BridgeCell.swift
//  STB
//
//  Created by XavierTanXY on 25/4/19.
//  Copyright © 2019 STB. All rights reserved.
//

import Foundation
import UIKit

class BridgeCell: UITableViewCell {
    
    @IBOutlet weak var idLbl: UILabel!
    @IBOutlet weak var ipLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(bridge: Bridge) {
        self.idLbl.text = bridge.bridgeID
        self.ipLbl.text = bridge.bridgeIP
    }
}
