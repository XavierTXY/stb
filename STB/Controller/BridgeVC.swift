//
//  BridgeVC.swift
//  STB
//
//  Created by XavierTanXY on 25/4/19.
//  Copyright © 2019 STB. All rights reserved.
//

import UIKit

class BridgeVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var selectedBridge: Bridge?
    var bridgesArray = [Bridge]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        let button1 = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refresh))
        self.navigationItem.rightBarButtonItem = button1
        self.navigationItem.title = "Discover Bridges"
        //Two ways to connect to bridge - discover or manually enter the details
        addHardCodedBridge()
//        discoverBridge()
    }
    
    // MARK: Discovery of Bridges
    @objc func refresh() {
        discoverBridge()
    }
  
    func addHardCodedBridge() {
        
//        let newBridge = Bridge(id: "000088FFFExxx", ip: "192.168.1.xxxx")
        let newBridge = Bridge(id: "xxx", ip: "192.168.xxx.xxx")
        
        self.bridgesArray.append(newBridge)
    }
    
    func discoverBridge() {
        
        let options: PHSBridgeDiscoveryOption = .discoveryOptionUPNP
        let bridgeDiscover = PHSBridgeDiscovery()
        
        bridgeDiscover.search(options) { (result, returnCode) in
            
            if returnCode == .success {
                
                for  (_, value) in result! {
                    
                    let newBridge = Bridge(id: value.uniqueId, ip: value.ipAddress)
                    self.bridgesArray.append(newBridge)
                }
                
                
            }
        }
    }
    
    // MARK: Tableview setup
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BridgeCell") as? BridgeCell
        cell?.selectionStyle = .none
        cell?.configureCell(bridge: bridgesArray[indexPath.row])
        return cell!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bridgesArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.selectedBridge = bridgesArray[indexPath.row]
        self.performSegue(withIdentifier: "ControlVC", sender: nil)
    }
    
    
    // MARK: Segue way
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ControlVC {
            destinationVC.selectedBridge = self.selectedBridge
            
        }
    }
    
    


}

