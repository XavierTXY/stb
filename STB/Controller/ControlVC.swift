//
//  ControlVC.swift
//  STB
//
//  Created by XavierTanXY on 25/4/19.
//  Copyright © 2019 STB. All rights reserved.
//

import UIKit

class ControlVC: UIViewController, PHSBridgeConnectionObserver {

    
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBOutlet weak var hueSlider: UISlider!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var temperateLbl: UILabel!
    @IBOutlet weak var timerBtn: UIButton!
    
    var bridge: PHSBridge! = nil
    var timer = Timer()
    
    var addition = true
    var increment = 1
    
    var prevTemp: Double?
    var currentTemp: Double?
    var firstTimeFetching = true
    
    var weatherInfo: String?
    
    var selectedBridge: Bridge?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        segmentedControl.setTitle("On", forSegmentAt: 0)
        segmentedControl.setTitle("Off", forSegmentAt: 1)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        guard selectedBridge == nil else {
            bridge = configureBridgeConnection(bridge: selectedBridge!)
            bridge.connect()
            
            return
        }
    }

    
    // MARK: Bridge Connection Setup
    func configureBridgeConnection(bridge: Bridge) -> PHSBridge {
        
        return PHSBridge.init(block: { (builder) in
            builder?.connectionTypes = .local
            builder?.ipAddress = bridge.bridgeIP
            builder?.bridgeID = bridge.bridgeID
            
            builder?.bridgeConnectionObserver = self
//            builder?.add(self as! PHSBridgeStateUpdateObserver)
            
        }, withAppName: "STB", withDeviceName: "iPhoneXR")
        
    }
    
    func bridgeConnection(_ bridgeConnection: PHSBridgeConnection!, handle connectionEvent: PHSBridgeConnectionEvent) {
        
        switch connectionEvent {
            case .couldNotConnect:
                showCouldNotConnectAlert()
            case .connected:
                 showConnected()
                break
            case .connectionLost:
                handleConnectionLostEvent()
                break
            case .connectionRestored:
                handleConnectionRestoredEvent()
                break
            case .disconnected:
                handleDisconnectedEvent()
                break
            case .notAuthenticated:
                handleNotAuthenticated()
                break
            case .linkButtonNotPressed:
                handleButtonNotPressed()
                break
            case .authenticated:
                handleAuthenticationEvent()
                break
            default:
                break
        }
    }
    
    func bridgeConnection(_ bridgeConnection: PHSBridgeConnection!, handleErrors connectionErrors: [PHSError]!) {
        //Handle Errors
    }

    // MARK: Fetch local temperature
    
    /* Update the hue brightness according to the local temperature.
    /  Currently, there are many ways to get weather data and update the lights,
    /  I choose to pull the weather date once every 30mins for simplicity, in the future we can setup observer to observe
    /  data changes in the weather source and update the light accordingly.
    */
    @objc func fetchTemp(){

        var diffTemp = 0.0
        let urlString = URL(string: "https://openweathermap.org/data/2.5/weather?q=Singapore&appid=b6907d289e10d714a6e88b30761fae22")

        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in

                if error != nil {

                    Alert.alert.showAlert(title: "Error", msg: error.debugDescription, object: self, dismiss: false)
                } else {

                    if let usableData = data {
                        do {

                            let json = try JSONSerialization.jsonObject(with: usableData, options: []) as! Dictionary<String, AnyObject>

                            if let weather = json["weather"] as? [Dictionary<String, AnyObject>] {
                                if let weatherInfo = weather[0]["main"] as? String {
                                    if let tempDegree = json["main"]!["temp"] as? Double {

                                        DispatchQueue.main.async {
                                            self.temperateLbl.text = "\(weatherInfo) - \(tempDegree) celsius"
                                        }
                                        
                                        
                                        self.prevTemp = tempDegree
                                        
                                        if self.currentTemp != nil && self.prevTemp != nil {
                                            
                                            
                                            let devices = self.getDevices()
                                            
                                            for device in devices {
                                                
                                                if let lightPoint = device as? PHSLightPoint {
                                                    
                                                    var lightstate: PHSLightState?
                                                    
                                                    /* Get the absolute diffrence of the temperature from 30mins ago and current temperature.
                                                    /  Use the absoulte * 6.35 because I used 0 as the minimum temperature and max is 40 celsius.
                                                    /  The max of brightness of hue is 254, so the ratio for every 1 celsius degree to 1 point of
                                                    /  brightness is 6.35 ( 254 / 40 ).
                                                    /  Whenever there is a change in temperature, it will change the brightness ONLY.
                                                    */
                                                    diffTemp = fabs((self.prevTemp! - self.currentTemp!)) * 6.35
                                                    
                                                    if (self.prevTemp!.isLess(than: self.currentTemp!)) {
                                                        lightstate = self.updateBrightnessPeriodically(brightValue: CGFloat(diffTemp), addition: false, existingBrightness: lightPoint.lightState.brightness)
                                                    } else {
                                                        lightstate = self.updateBrightnessPeriodically(brightValue: CGFloat(diffTemp), addition: true, existingBrightness: lightPoint.lightState.brightness)
                                                    }
                                                    
                                                    lightPoint.update(lightstate, allowedConnectionTypes: .local, completionHandler: { (response, errorArray, returnCode) in
                                                        
                                                        self.updateSliderValues(lightPoint: lightPoint)
                                                        
                                                        if errorArray != nil {
                                                            
                                                            for err in errorArray! {
                                                                
                                                                if self.timer != nil {
                                                                    self.timer.invalidate()
                                                                }
                                                                
                                                                Alert.alert.showAlert(title: "Error Occurred", msg: err.debugDescription, object: self, dismiss: false)
                                                                
                                                                
                                                            }
                                                        }
                                                    })
                                                }
                                                
                                                
                                            }
                                            
                                            
                                        }
                                        
                                        if !self.firstTimeFetching {

                                            self.currentTemp = self.prevTemp
                                            
                                        }
                                        
                                        self.firstTimeFetching = false
                    
                                        
                                    }
                                }
                                
                            }

                            


                        } catch let error as NSError {

                            Alert.alert.showAlert(title: "Error", msg: error.debugDescription, object: self, dismiss: false)
                        }
                    }
                }
            }

            task.resume()

        }
    }
    
    //Get all the devices that are conencted to that bridge
    func getDevices() -> [PHSDevice] {
        
        if let devices = bridge.bridgeState.getDevicesOf(.light) as? [PHSDevice] {
            return devices
        } else {
            return [PHSDevice]()
        }
    }
    
    // MARK: Update UI
    func updateSliderValues(lightPoint: PHSLightPoint) {
        DispatchQueue.main.async {
            self.brightnessSlider.setValue(lightPoint.lightState.brightness.floatValue, animated: false)
            self.hueSlider.setValue(lightPoint.lightState.hue.floatValue, animated: false)
            
        }
    }
    
    // MARK: Update HUE Setup - hue, brightness
    func updateLighting(turnOn: Bool, lightPoint: PHSLightPoint) {
        let lightState = PHSLightState()
        if turnOn {
            segmentedControl.selectedSegmentIndex = 0
            lightState.on = true
        } else {
            segmentedControl.selectedSegmentIndex = 1
            lightState.on = false
        }
        
        
        lightPoint.update(lightState, allowedConnectionTypes: .local, completionHandler: { (response, errorArray, returnCode) in
            
            if errorArray != nil {
                
                for err in errorArray! {
                    
                    Alert.alert.showAlert(title: "Error Occurred", msg: err.debugDescription, object: self, dismiss: false)
                }
            }
        })
        
    }
    
    func updateLightWithRandomColors() -> PHSLightState {
        let lightState = PHSLightState()
        
        let randomHue = Int.random(in: 0..<65535)
        let randomBrightness = Int.random(in: 0..<254)
        
        lightState.on = true
        segmentedControl.selectedSegmentIndex = 0
        lightState.hue = randomHue as NSNumber
        lightState.brightness = randomBrightness as NSNumber
        
        return lightState
    }
    
    func updateBrightnessPeriodically(brightValue: CGFloat, addition: Bool, existingBrightness: NSNumber) -> PHSLightState {
        let lightState = PHSLightState()
        
        lightState.on = true
        
        DispatchQueue.main.async {
            self.segmentedControl.selectedSegmentIndex = 0
        }
        

        if addition {
            if !( (existingBrightness.doubleValue + Double(brightValue) ) >= 254 ) {
                lightState.brightness = NSNumber(value: existingBrightness.doubleValue + Double(brightValue))
            }
            
        } else {

            if !(existingBrightness.doubleValue - Double(brightValue) ).isLess(than: 0.0) {
                lightState.brightness = NSNumber(value: existingBrightness.doubleValue - Double(brightValue))
            }
            
        }
        
        return lightState
    }
    
    func updateBrightness(brightValue: CGFloat) -> PHSLightState {
        let lightState = PHSLightState()
        
        lightState.on = true
        DispatchQueue.main.async { // Correct
            self.segmentedControl.selectedSegmentIndex = 0
        }
        lightState.brightness = brightValue as NSNumber
        
        return lightState
    }
    
    func updateLightWithColors(colorValue: CGFloat) -> PHSLightState {
        let lightState = PHSLightState()
        
        lightState.on = true
        DispatchQueue.main.async { // Correct
            self.segmentedControl.selectedSegmentIndex = 0
        }
        lightState.hue = colorValue as NSNumber
        
        return lightState
    }
    
    // MARK: Action methods
    @IBAction func randomizeLights(_ sender: Any) {
        
        let devices = getDevices()
        
        for device in devices {
            
            if let lightPoint = device as? PHSLightPoint {
                let lightstate = self.updateLightWithRandomColors()
                
                lightPoint.update(lightstate, allowedConnectionTypes: .local, completionHandler: { (response, errorArray, returnCode) in
            
                    
                    self.updateSliderValues(lightPoint: lightPoint)
                    
                    if errorArray != nil {
                        
                        for err in errorArray! {
                            
                            Alert.alert.showAlert(title: "Error Occurred", msg: err.debugDescription, object: self, dismiss: false)
                        }
                    }
                })
            }
        }
        
    }
    
    @IBAction func slideToUpdateBrightness(_ sender: Any) {
        let devices = getDevices()
        
        for device in devices {
            
            if let lightPoint = device as? PHSLightPoint {
                
                let lightstate = self.updateBrightness(brightValue: CGFloat(brightnessSlider.value))
                
                lightPoint.update(lightstate, allowedConnectionTypes: .local, completionHandler: { (response, errorArray, returnCode) in
                    
                    if errorArray != nil {
                        
                        for err in errorArray! {
                            
                            Alert.alert.showAlert(title: "Error Occurred", msg: err.debugDescription, object: self, dismiss: false)
                        }
                    }
                })
            }
        }

    }
    @IBAction func slideToUpdateColors(_ sender: Any) {
        
        let devices = getDevices()
        
        for device in devices {
            
            if let lightPoint = device as? PHSLightPoint {
   
                let lightstate = self.updateLightWithColors(colorValue: CGFloat(hueSlider.value))
                
                lightPoint.update(lightstate, allowedConnectionTypes: .local, completionHandler: { (response, errorArray, returnCode) in
                    
                    if errorArray != nil {
                        
                        for err in errorArray! {
                            
                            Alert.alert.showAlert(title: "Error Occurred", msg: err.debugDescription, object: self, dismiss: false)
                        }
                    }
                })
            }
        }
        
    }
    
    @IBAction func turnOnOFFLighting(_ sender: Any) {
        let devices = getDevices()
        
        for device in devices {
            
            if let lightPoint = device as? PHSLightPoint {
                
                switch lightPoint.lightState.on {
                    case 0:
                        self.updateLighting(turnOn: true, lightPoint: lightPoint)
                    case 1:
                        self.updateLighting(turnOn: false, lightPoint: lightPoint)
                    default:
                        break
                }
            }
        }
    }
    
    @IBAction func setColorAccoringWeatherTapped(_ sender: Any) {
        
        if timer != nil {
            timer.invalidate() // just in case this button is tapped multiple times
            self.timerBtn.setTitle("Set Colors According to Weather", for: .normal)
        } else {
            // start the timer
            //Setting time interval for 30mins interval - it will update the light for every 30mins
            timer = Timer.scheduledTimer(timeInterval: 1800, target: self, selector: #selector(fetchTemp), userInfo: nil, repeats: true)
            self.timerBtn.setTitle("Stop Setting Colors Automatically", for: .normal)
            
        }

    }
    
    @IBAction func backTapped(_ sender: Any) {
        disconnectBridge()
    }
    
    @IBAction func disconnectBridgeTapped(_ sender: Any) {
        disconnectBridge()
    }
    
    func disconnectBridge() {
        let alert = UIAlertController(title: "Disconnect Bridge", message: "Are you sure you want to disconnect?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Disconnect", style: .destructive, handler: { (action) in
            self.bridge.disconnect()
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
}


