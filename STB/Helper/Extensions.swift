//
//  Extensions.swift
//  STB
//
//  Created by XavierTanXY on 25/4/19.
//  Copyright © 2019 STB. All rights reserved.
//

import Foundation
import UIKit

extension ControlVC {
    
    func showCouldNotConnectAlert() {
        Alert.alert.showAlert(title: "Connectivity Issue", msg: "There is an issue connecting to the bridge", object: self, dismiss: true)
    }
    
    func handleConnectionLostEvent() {
        Alert.alert.showAlert(title: "Connectivity Issue", msg: "Please try to connect again or connect to other bridge", object: self, dismiss: true)
    }
    
    func showConnected() {
        Alert.alert.showAlert(title: "Sucessfully Connected", msg: "You have successfully connected", object: self, dismiss: false)
    }

    func handleConnectionRestoredEvent() {
        Alert.alert.showAlert(title: "Connectivity Restored", msg: "Connectivity has been restored", object: self, dismiss: false)
    }

    func handleDisconnectedEvent() {
        Alert.alert.showAlert(title: "Bridge Disconnected", msg: "You have been disconnected from the bridge", object: self, dismiss: true)
    }

    func handleNotAuthenticated() {
        Alert.alert.showAlert(title: "Connectivity Issue", msg: "Please pushlink with the bridge", object: self, dismiss: false)
    }
    func handleButtonNotPressed() {
        let alert = UIAlertController(title: "Connectivity Issue", message: "Please remember to press the pushlink button", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Pressed", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true)
    }

    func handleAuthenticationEvent() {
        Alert.alert.showAlert(title: "Successfully Authenticated", msg: "You have connected to the bridge", object: self, dismiss: false)
    }
}
