//
//  Bridge.swift
//  STB
//
//  Created by XavierTanXY on 25/4/19.
//  Copyright © 2019 STB. All rights reserved.
//

import Foundation

class Bridge {
    
    private var _bridgeID: String?
    private var _bridgeIP: String?
    
    var bridgeID: String {
        
        get {
            return _bridgeID!
        }
        
        set(newID) {
            _bridgeID = newID
        }
    }
    
    var bridgeIP: String {
        
        get {
            return _bridgeIP!
        }
        
        set(newIP) {
            _bridgeIP = newIP
        }
    }
    
    
    init(id: String, ip: String) {
        self.bridgeID = id
        self.bridgeIP = ip
    }
}
