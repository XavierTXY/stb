//
//  Alert.swift
//  STB
//
//  Created by XavierTanXY on 27/4/19.
//  Copyright © 2019 STB. All rights reserved.
//

import Foundation
import UIKit

class Alert {
    
    static let alert = Alert()
    
    init() {}
    
    func showAlert(title: String, msg: String, object: UIViewController, dismiss: Bool) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        if dismiss {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                object.dismiss(animated: true, completion: nil)
            }))
        } else {
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        }
        
        
        object.present(alert, animated: true)
    }
}

